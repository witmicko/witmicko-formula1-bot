const configUtil = require('./lib/ConfigUtils');
const botComms = require('./lib/F1Advisor');


module.exports = async() => {
  const args = process.argv.slice(2);

  botComms.processCliInput(args);


  // save config on exit
  process.on('exit', () => configUtil.saveConfig());
  process.on('SIGINT', () => configUtil.saveConfig())
};