const expect = require('chai').expect;

const ergast = require('../lib/ErgastApi');

describe('Test ErgastApi', () => {
  it("Should get correct race", async () => {
    let race = await ergast.getRace(2018, 13);
    expect(race).to.exist;
    expect(race.raceName).to.equal("Belgian Grand Prix");
  })
});