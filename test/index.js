const expect = require('chai').expect;

const ErgastApi = require('../lib/ErgastApi');

  describe('Test ErgastApi utils', () => {
    it("should build correct url 1", () => {
      let urlFixture = "user/should/become/bot";
      let newUrl = ErgastApi.makeUrl(urlFixture, {user: "bot"});
      expect(newUrl).to.equal("bot/should/become/bot")
    });

    it("should build correct url 2", () => {
      let urlFixture = "one/two/three/four";
      let newUrl = ErgastApi.makeUrl(urlFixture, {
        one: "1",
        two: "2",
        three: "3",
        four: "4"
      });
      expect(newUrl).to.equal("1/2/3/4")
    })
  });