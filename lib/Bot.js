const api = require('./ErgastApi');
const config = require('./ConfigUtils');
const moment = require('moment');

/**
 * returns race schedule
 * for season set in the config
 * @returns {Promise<*>}
 */
async function getSchedule(season) {
  if (!season) season = config.getSeason();

  let schedule = await api.getSchedule(season);
  return schedule.Races;
}

async function getRace(season, round) {
  if (!season) season = config.getSeason();
  if (!round) round = config.getRound();

  return await api.getRace(season, round);
}

async function getWinner(season, round) {
  if (!season) season = config.getSeason();
  if (!round) round = config.getRound();

  return await api.getWinner(season, round);
}

async function getPoleQuali(season, round) {
  if (!season) season = config.getSeason();
  if (!round) round = config.getRound();

  return await api.getPoleQuali(season, round);
}

async function getFastestLap(season, round) {
  if (!season) season = config.getSeason();
  if (!round) round = config.getRound();

  return await api.getFastestLap(season, round);
}

/**
 * returns next race details
 * for season set in the config
 * @returns {Promise<void>}
 */
async function getNextVenue() {
  let currentDate = new Date();
  let races = await getSchedule(currentDate.getFullYear());

  let lastRound = 1;
  for (const race of races) {
    let roundNumber = parseInt(race.round);
    let raceDate = moment(race.date);

    if (raceDate.isAfter(currentDate) && roundNumber === (lastRound + 1)) {
      return race;
    } else if (raceDate.isBefore(currentDate)) {
      lastRound = roundNumber
    }
  }
}


module.exports = {
  getFastestLap,
  getNextVenue,
  getPoleQuali,
  getRace,
  getSchedule,
  getWinner
};