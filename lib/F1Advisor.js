const bot = require('./Bot');
const config = require('./ConfigUtils');
const utils = require('./utils');
const moment = require('moment');

let discovery = {
  next: false,
  season: false,
  round: false
};


function processCliInput(input) {
  for (const word of input) {
    checkForSeason(word);
    checkForRound(word);
    checkForNext(word);
  }

  processDiscovery(discovery);

}

/**
 * Processes what was discovered from the input, runs appropriate queries
 */
async function processDiscovery(d) {
  let race;

  if (discovery.next) {
    race = await bot.getNextVenue();
    printNextRace(race);
  } else {
    if (d.season && d.round) {
      race = await bot.getRace();
      printRaceResult(race);
    }
  }
}


function printNextRace(race) {
  let raceDate = moment(race.date).format("DD MM YYYY");
  let message = `Next race is ${race.raceName}, it will take place on ${race.Circuit.circuitName} in ${race.Circuit.Location.country} on ${raceDate}.
                 For more information check ${race.url}`;

  console.log(message);
  return message;
}

function printRaceResult(race) {
  let circuit = race.Circuit;
  let raceDate = moment(race.date).format("DD MM YYYY");

  let message = `${race.raceName} took place on ${circuit.circuitName} in ${circuit.Location.country} on ${raceDate}`;
  console.log(message);
  return message
}


function checkForNext(word) {
  if (word === "next") {
    discovery.next = true;
  }
}

/**
 * going of an assumption that if there is a year in the query it specifies season
 * @param word
 */
function checkForSeason(word) {
  if (utils.isInt(word)) {
    let maybeYear = parseInt(word);
    if (maybeYear > 1949 && maybeYear < 2050) {
      config.setSeason(maybeYear);
      console.log("Season ", maybeYear);
      discovery.season = true;
    }
  }
}


/**
 * going of an assumption that if there is a number between 1 and 30 it most likely means which round
 * @param word
 */
function checkForRound(word) {
  if (utils.isInt(word)) {
    let maybeRound = parseInt(word);
    if (maybeRound > 0 && maybeRound < 30) {
      config.setRound(maybeRound);
      console.log("Round ", maybeRound);
      discovery.round = true;
    }
  }
}

module.exports = {
  processCliInput,
};