const config = require('../bin/config');
const path = require("path");
const fs = require('fs');

function getSeason() {
  return config.season;
}

function setSeason(season) {
  config.season = season;
}
function setRound(round) {
  config.round = round;
}

function getRound(round) {
  return config.round;
}
async function saveConfig() {
  let configPath = path.resolve(__dirname, "../bin/config.json");
  fs.writeFileSync(configPath, JSON.stringify(config));
}

module.exports = {
  getSeason,
  setSeason,
  setRound,
  getRound,
  saveConfig
};