const request = require('request-promise');
const urls = require('../constants/ergast-enpoints');


/**
 * https://ergast.com/mrd/methods/schedule/
 */
async function getSchedule(season) {
  let url = makeUrl(urls.RACE_SCHEDULE, {
    "SEASON": season,
  });
  let response = await makeRequest(url);
  return response.MRData.RaceTable;
}

/**
 * gets race for given series season and round
 * http://ergast.com/api/f1/2008/5
 * @param series
 * @param season
 * @param round
 * @returns {Promise<*>}
 */
async function getRace(season, round) {
  let url = makeUrl(urls.RACE, {
    "SEASON": season,
    "ROUND": round
  });

  let response = await makeRequest(url);
  return response.MRData.RaceTable.Races[0];
}


async function getWinner(season, round){
  let url = makeUrl(urls.RACE, {
    "SEASON": season,
    "ROUND": round
  });

  let response = await makeRequest(url);
  return response.MRData.RaceTable.Races[0];
}

async function getPoleQuali(season, round){
  let url = makeUrl(urls.RACE, {
    "SEASON": season,
    "ROUND": round
  });

  let response = await makeRequest(url);
  return response.MRData.RaceTable.Races[0];
}

async function getFastestLap(season, round){
  let url = makeUrl(urls.RACE, {
    "SEASON": season,
    "ROUND": round
  });

  let response = await makeRequest(url);
  return response.MRData.RaceTable.Races[0];
}

/**
 * util to make http request and handle errors
 * @param url
 * @returns {Promise<any>}
 */
async function makeRequest(url) {
  try {
    let response = await request(url);
    return JSON.parse(response)
  } catch (err) {
    console.log("Sorry something went wrong \n " + err.message);
  }
}

/**
 * util function to make url
 * @param url
 * @param replacers
 * @returns {*}
 */
function makeUrl(url, replacers) {
  for (const key of Object.keys(replacers)) {
    url = url.replace(key, replacers[key]);
  }
  return url;
}


module.exports = {
  getFastestLap,
  getSchedule,
  getRace,
  getWinner,
  getPoleQuali,
  makeUrl
};