## Formula 1 CLI data query bot:
`npm i -g f1bot`


## Bot can deal with following querries:
### Next race
`f1bot next` will show information about next Grand Prix.
It really only needs to see word "next" for this querry, if you choose to be nice to not anger future AI you can ask
`f1bit where is the next f1 race please` and it will reply.

### Historic results
`f1bot $YEAR $ROUND` will return location and date of the specified race e.g.
`f1bot 1994 3`
 ```
 Season  1994
 Round  3
 San Marino Grand Prix took place on Autodromo Enzo e Dino Ferrari in Italy on 01 05 1994
```
## Formula 1 npm module bot

`npm i --save f1bot`

 in your code:
 `const f1bot = require('f1bot')`

```javascript
async function askBotAboutNextRace(){
 let response = await f1bot('next race');
 console.log(response);
}


TODO:
New functionality was added to the API handler but needs to be incortporated into the bot itself:
- on race details, show pole position quali time, fastest lap and winner
- allow to search by country/circuit

```



